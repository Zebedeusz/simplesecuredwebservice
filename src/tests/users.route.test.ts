import * as request from 'supertest';
import App from '../app';
import UsersRoute from 'routes/users.route';
import { expect } from 'chai'

const VALID_USER_INPUT = {
  username: 'some_user',
  password: 'pass'
};

describe('Testing users endpoint', () => {
  let app: App;
  let usersRoute: UsersRoute;

  before(() => {
    usersRoute = new UsersRoute();
    app = new App([usersRoute]);
  });

  it('should respond with 201 and created user in body', (done: MochaDone) => {
    request(app.getServer())
      .post(`${usersRoute.path}`)
      .send(VALID_USER_INPUT)
      .expect(201)
      .end((err, res: request.Response) => {
        expect(res.body).not.to.be.undefined.and.not.to.be.empty;
        expect(res.body.username).to.be.equal(VALID_USER_INPUT.username);
        expect(res.body.password).to.be.equal(VALID_USER_INPUT.password);
        done(err);
      });
  });

  it('should respond with 400 given no user in input', () => {
    return request(app.getServer())
      .post(`${usersRoute.path}`)
      .send({password: 'x'})
      .expect(400);
  });

  it('should respond with 400 given no token in source input', () => {
    return request(app.getServer())
      .post(`${usersRoute.path}`)
      .send({username: 'x'})
      .expect(400);
  });

  it('should respond with 400 given empty input', () => {
    return request(app.getServer())
      .post(`${usersRoute.path}`)
      .expect(400);
  });

  it('should respond with 404 to GET route', () => {
    return request(app.getServer())
      .get(`${usersRoute.path}`)
      .expect(404);
  });
});
