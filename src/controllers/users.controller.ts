import { NextFunction, Request, Response } from 'express';
import HttpError from '../errors/http.error';

class UsersController {
  public createUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
      res.status(201).json('');
    } catch (error) {
      next(error);
    }
  }
}

export default UsersController;
