import * as joi from '@hapi/joi'

export const userSchema =
  joi.object().keys({
    username: joi.string().required(),
    password: joi.string().required()
  });