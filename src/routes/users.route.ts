import { Router } from 'express';
import UsersController from '../controllers/users.controller';
import Route from '../interfaces/route.interface';
import { createValidator } from 'express-joi-validation'
import { userSchema } from './validation/schemas/user.schema';

const validator = createValidator()

class UsersRoute implements Route {
  public path = '/users';
  public router = Router();
  public usersController = new UsersController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}`, validator.body(userSchema), this.usersController.createUser);
  }
}

export default UsersRoute;